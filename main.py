# -*- coding: utf-8 -*-
'''
Created on Monday 30-09-2019
Copyright (©) Applied Drone Innovations B.V.
Author: Tommaso Brandirali
Email: tommaso@adinnovations.nl
'''

#****************************************************************************************
# Imports
#****************************************************************************************

import cv2
import argparse
import time
import numpy as np
import easygui as gui
import matplotlib.pyplot as plt

#****************************************************************************************
# Default Variables for Testing
#****************************************************************************************

# Default reference image: orchids from Hazeu
default_reference_path = "resources/041.jpg"

#****************************************************************************************************
# Main Code
#****************************************************************************************************

class ColourNormalizer:

    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Class Variables
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    # -- Default threshold for the channel mean difference between the two images --
    default_threshold = 5.0

    def __init__(self, reference: np.array, threshold: float = None, testing: bool = None):
        self.reference = reference
        self.threshold = threshold if threshold else self.default_threshold
        self.testing = testing if testing else False

    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Define colorspace transformation shortcuts
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    @staticmethod
    def recolor(colorspace):
        switcher = {
            "RGB": cv2.COLOR_BGR2BGRA,
            "GRAY": cv2.COLOR_BGR2GRAY,
            "XYZ": cv2.COLOR_BGR2XYZ,
            "LUV": cv2.COLOR_BGR2LUV,
            "HSV": cv2.COLOR_BGR2HSV,
            "LAB": cv2.COLOR_BGR2LAB
        }
        return switcher.get(colorspace, None)

    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Load and resize image from path
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    @staticmethod
    def load_image(image_path, reduce_x=0.4, reduce_y=0.4):

        print(f'[ ColourNormalizer ][ load_image() ] - loading image from {image_path}')
        return cv2.resize(cv2.imread(image_path, -1), None, fx=reduce_x, fy=reduce_y)

    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
    # Normalize pixel color values based on target
    #++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

    def normalize(self, target, threshold=None):

        start_time = time.time()

        # --- Set default threshold if no custom threshold chosen ---
        if threshold is None:
            threshold = self.default_threshold

        # --- Normalize colour and lighting within single picture ---
        ref_norm = np.zeros((reference.shape[0], reference.shape[1]))
        ref_norm = cv2.normalize(reference, ref_norm, 0, 255, cv2.NORM_MINMAX)

        tgt_norm = np.zeros((target.shape[0], target.shape[1]))
        tgt_norm = cv2.normalize(target, tgt_norm, 0, 255, cv2.NORM_MINMAX)

        # --- Transpose to Lab colour space ---
        ref_lab = cv2.cvtColor(ref_norm, self.recolor("LAB"))
        tgt_lab = cv2.cvtColor(tgt_norm, self.recolor("LAB"))

        ### CLAHE EQUALIZATION: REMOVES ENVIRONMENTAL BIAS ON COLOUR, BUT DECREASES SATURATION.
        ### NOT REQUIRED FOR EFFECTIVE NORMALIZATION

        # # --- Apply Contrast Limited Adaptive Histogram Equalization ---
        # clahe = cv2.createCLAHE(clipLimit=2.0)
        # ref_lab[..., 0] = clahe.apply(ref_lab[..., 0])
        # tgt_lab[..., 0] = clahe.apply(tgt_lab[..., 0])

        # --- Calculate mean values per single channel ---
        ref_means = cv2.mean(ref_lab[:, :, ])
        tgt_means = cv2.mean(tgt_lab[:, :, ])

        # --- Create matrix of means' differences, from 1D to 3D ---
        diff = np.array([ref_means[i] - tgt_means[i] for i in range(3)])    # 1D
        sum_diff = np.sum(diff)
        diff = np.tile(diff, (len(target), len(target[0]), 1))              # 3D

        # --- create mask for non-negative pixel thresholding ---
        mask = (tgt_lab + diff > 0).astype(np.uint8)
        masked_diff = mask * diff

        if self.testing:
            print(f'[ ColourNormalizer ][ two_picture_normalization() ] - reference image channel means = {ref_means},\n'
              f'[ ColourNormalizer ][ two_picture_normalization() ] - target image channel means = {tgt_means},\n'
              f'[ ColourNormalizer ][ two_picture_normalization() ] - cumulative difference = {sum_diff}')

        # --- Apply normalization if total colour difference above threshold ---
        if abs(sum_diff) > threshold:
            tgt_lab += masked_diff.astype(np.uint8)

        # --- Transpose back to BGR colour space ---
        tgt_color = cv2.cvtColor(tgt_lab, cv2.COLOR_LAB2BGR)

        time_elapsed = time.time() - start_time

        if self.testing:
            print(f'[ ColourNormalizer ][ two_picture_normalization() ] - time elapsed: {time_elapsed}')

        return tgt_color

#****************************************************************************************************
# Test Code
#****************************************************************************************************

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Show image
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

def imshow(name,img):

    if len(img.shape) >= 3 and img.shape[2] == 3:
        img = img[:, :, ::-1]
    plt.figure()
    plt.title(name)
    plt.imshow(img)

#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
# Main method for testing purposes
#++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

if __name__ == '__main__':

    # --- Setup command line argument parsing ---
    parser = argparse.ArgumentParser(description="Normalizes the color of an image based on a reference image.")
    parser.add_argument('--reference', '-r', help='path to the reference image')
    parser.add_argument('--target', '-t', help='path to the target image')
    parser.add_argument('--threshold', '-d', help='threshold for the channel mean difference')
    args = parser.parse_args()

    # --- Parse arguments, set values ---
    if args.reference:
        reference_path = args.reference
    else:
        reference_path = default_reference_path

    if args.target:
        target_path = args.target
    else:
        target_path = gui.fileopenbox("Choose the image to be loaded:")
        if target_path == None:
            print("[ ColourNormalizer ][ MAIN] -  ERROR: Execution interrupted, terminating...")
            exit(1)

    # --- Load and resize images
    reference = ColourNormalizer.load_image(reference_path)
    target = ColourNormalizer.load_image(target_path)

    # --- Instantiate Colour Normalizer, apply normalization ---
    colour_normalizer = ColourNormalizer(reference, testing=True)
    normalized = colour_normalizer.normalize(target)

    # --- Show images ---
    imshow('Reference Image', reference)
    imshow('Original Image', target)
    imshow('Normalized Image', normalized)
    plt.show()
